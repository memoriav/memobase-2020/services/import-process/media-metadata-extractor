# Media Metadata Extractor

This service requests media metadata from two dedicated services,
[media-indexer](https://gitlab.switch.ch/memoriav/memobase/services/indexer)
and
[media-indexer-helper](https://gitlab.switch.ch/memoriav/memobase/services/histogram), and enriches the record metadata
with the returned media metadata. media-indexer and media-indexer-helper use different tools to extract the requested
metadata:

- Siegfried: Mime-Type and PRONOM-id
- ffmpeg (especially ffprobe): AV metadata extraction; AV validation
- imagemagick (especially identify and convert): Image metadata extraction; image validation

If errors occur during the analysis, Media Metadata Extractor tries two enrich the record as much as possible but will
nevertheless issue a WARNING report. If all went well, a SUCCESS is propagated.

While internally the same, there are actually two deployments of Media Metadata Extractor running. One for images (fed
by input topic import-process-image-enrichment) and one for AV media (reading from input topic
import-process-av-enrichment).

## Configuration

In order to work correctly, some environment variables have to be set:

* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap server addresses
* `APPLICATION_ID`: Id used by Kafka Streams application (
  see [Kafka documentation](https://kafka.apache.org/documentation/#streamsconfigs_application.id) for details)
* `TOPIC_IN`: Name of Kafka topic where messages are read from
* `TOPIC_OUT`: Name of Kafka topic where messages are written to (without environment postfix)
* `TOPIC_PROCESS`: Name of Kafka topic where status reports are written to
* `INDEXER_HOST`: Address of indexer service
* `INDEXER_CONNECT_TIMEOUT_MS`: Time in milliseconds after which a connection timeout occurs
* `INDEXER_READ_TIMEOUT_MS`: Duration in milliseconds in which a response from the indexer is expected; consider that
  the processing of a large media file can take a bit of time...
* `CONSUMER_MAX_POLL_INTERVAL_MS`: Maximum time of consumer idleness in milliseconds; after this period the consumer is
  considered failed (
  see [Kafka documentation](https://kafka.apache.org/documentation/#consumerconfigs_max.poll.interval.ms) for details)
* `CONSUMER_MAX_POLL_RECORDS`: Maximum number of records returned in a single call to poll() (
  see [Kafka documentation](https://kafka.apache.org/documentation/#consumerconfigs_max.poll.records) for details)
* `PARSER_ACTIONS_REMOTE`: Comma-separated list of actions which should be performed by the indexer when analysing a
  remote media file (see below for allowed actions)
* `PARSER_ACTIONS_LOCAL`: Comma-separated list of actions which should be performed by the indexer when analysing a
  locally available media file
* `THROTTLING_RATE_FOR_COLLECTIONS`: Comma-separated list of collections whose processing should be throttled (see below
  for details)
* `SECURITY_PROTOCOL: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
* `SSL_KEYSTORE_TYPE: The file format of the key store file. This is optional for client. 
* `SSL_KEYSTORE_LOCATION: The location of the key store file. This is optional for client and can be used for two-way authentication for client
* `SSL_KEYSTORE_KEY`: Private key in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `SSL_KEY_PASSWORD`
* `SSL_KEYSTORE_CERTIFICATE_CHAIN`: Certificate chain in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with a list of X.509 certificates
* `SSL_TRUSTSTORE_TYPE: The file format of the trust store file. The values currently supported by the default `ssl.engine.factory.class` are `JKS`, `PKCS12` and `PEM`
* `SSL_TRUSTSTORE_LOCATION: The location of the trust store file
* `SSL_TRUSTSTORE_CERTIFICATES`: Trusted certificates in the format specified by `SSL_TRUSTSTORE_TYPE`. Default SSL engine factory supports only PEM format with X.509 certificates
* `SSL_KEY_PASSWORD`: The password of the private key in the Kafka key store file or the PEM key specified in `SSL_KEYSTORE_KEY`.

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Key store file in the format defined in `SSL_KEYSTORE_TYPE` and at the path defined in `SSL_KEYSTORE_LOCATION`
* Key store certificate chain in PEM format at the path defined in `SSL_KEYSTORE_CERTIFICATE_CHAIN`
* Key store key in PEM format at the path defined in `SSL_KEYSTORE_KEY`
* Trust store file in the format defined in `SSL_TRUSTSTORE_TYPE` and at the path defined in `SSL_TRUSTSTORE_LOCATION`
* Trust store certificates in PEM format at the path defined in `SSL_TRUSTSTORE_CERTIFICATES`

## Possible actions

* `siegfried`: Identify mime-type and PRONOM-id with [Siegfried](https://github.com/richardlehane/siegfried)
* `identify`: Run ImageMagick's [`identify`](https://imagemagick.org/script/identify.php) subcommand
* `ffprobe`: Run ffmpeg's [`ffprobe`](https://ffmpeg.org/ffprobe.html) subcommand
* `histogram`: Create a histogram from the analysed image
* `validateimage`: Validate audio or video file with [ImageMagick](https://imagemagick.org)
* `validateav`: Validate audio or video file with [ffmpeg](https://ffmpeg.org)
* `exif`: Extract EXIF data with [ExifTool](https://exiftool.org)

## Throttling

Some data providers set restrictions on how much requests are processed with a certain time span. For these cases, you
can use the `THROTTLING_RATE_FOR_COLLECTIONS` setting to cut the maximum amount of requests _per minute_ sent to the
provider. Use the follwoing syntax:

```sh
THROTTLING_RATE_FOR_COLLECTIONS=afz-001:10,baz-001:7
```

In this example, `afz-001` and `baz-001` are the collection ids. `10` and `7` are the respective maximum requests per
minute.
