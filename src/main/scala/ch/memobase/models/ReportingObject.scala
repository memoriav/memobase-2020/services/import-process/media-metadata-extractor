/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.models

import ch.memobase.AppSettings

import java.text.SimpleDateFormat
import java.util.Calendar

sealed trait ReportStatus {
  val value: String
}

case object ReportSuccess extends ReportStatus {
  override val value: String = "SUCCESS"
}

case object ReportWarning extends ReportStatus {
  override val value: String = "WARNING"
}

case object ReportIgnore extends ReportStatus {
  override val value: String = "IGNORE"
}

case object ReportFatal extends ReportStatus {
  override val value: String = "FATAL"
}

case class ReportingObject(
    id: String,
    status: ReportStatus,
    message: String
) extends AppSettings {
  import ReportingObject.createTimestamp

  override def toString: String =
    ujson.write(
      ujson.Obj(
        ("step", reportingStepName),
        ("stepVersion", getClass.getPackage.getImplementationVersion),
        ("timestamp", createTimestamp),
        ("id", id),
        ("status", status.value),
        ("message", message)
      )
    )
}

object ReportingObject {
  private val dateFormatter = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS")

  def createTimestamp: String =
    dateFormatter.format(Calendar.getInstance().getTime)
}
