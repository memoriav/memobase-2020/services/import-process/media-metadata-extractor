/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.models

import java.util.UUID.randomUUID
import scala.collection.mutable.ListBuffer
import scala.math.BigInt.long2bigInt
import java.util.UUID

sealed trait NtriplesObject
sealed trait RdfID extends NtriplesObject

class Literal(
    value: String,
    indicator: Option[String],
    indicatorType: IndicatorType
) extends NtriplesObject {
  override def toString: String = indicator match {
    case Some(i) =>
      indicatorType match {
        case LanguageIndicator => s""""${Literal.normalize(value)}"@$i"""
        case DatatypeIndicator => s""""${Literal.normalize(value)}"^^<$i>"""
      }
    case None => s""""${Literal.normalize(value)}""""
  }

}

object Literal {
  def apply(value: String): Literal =
    new Literal(value, None, LanguageIndicator)

  def apply(value: String, indicator: String): Literal =
    new Literal(value, Some(indicator), LanguageIndicator)

  def apply(
      value: String,
      indicator: String,
      indicatorType: IndicatorType
  ): Literal = new Literal(value, Some(indicator), indicatorType)

  private def normalize(value: String): String =
    value.replaceAll("[\n\r]+", "\\\\n")
}

sealed trait IndicatorType
case object LanguageIndicator extends IndicatorType
case object DatatypeIndicator extends IndicatorType

class URI(value: String) extends RdfID {
  override def toString: String = s"<${value}>"
}

class BNodeID(value: UUID) extends NtriplesObject with RdfID {
  override def toString: String = s"_:${value}"
}

class RDFResource(
    val uri: RdfID
) extends NtriplesObject {
  var flushed = false
  private val statements = new ListBuffer[(String, NtriplesObject)]

  def addLiteral(predicate: String, literal: String): RDFResource = {
    statements.addOne((predicate, Literal(literal)))
    this
  }

  def addLiteral(
      predicate: String,
      literal: String,
      indicator: String
  ): RDFResource = {
    statements.addOne((predicate, Literal(literal, indicator)))
    this
  }

  def addLiteral(
      predicate: String,
      literal: String,
      indicator: String,
      indicatorType: IndicatorType
  ): RDFResource = {
    statements.addOne((predicate, Literal(literal, indicator, indicatorType)))
    this
  }

  def addURI(predicate: String, uri: String): RDFResource = {
    statements.addOne((predicate, URI(uri)))
    this
  }

  def addURI(predicate: String, id: RdfID): RDFResource = {
    statements.addOne((predicate, id))
    this
  }

  def addResource(predicate: String, resource: RDFResource): RDFResource = {
    statements.addOne((predicate, resource))
    this
  }

  def flush: String = {
    if (this.flushed) {
      ""
    } else {
      this.flushed = true
      statements
        .map((p, o) =>
          o match {
            case o: URI     => s"$uri <${p}> $o .\n"
            case o: RdfID   => s"$uri <${p}> $o .\n"
            case o: Literal => s"$uri <${p}> $o .\n"
            case o: RDFResource =>
              s"$uri <${p}> ${o.uri} .\n${o.flush}"
          }
        )
        .mkString
    }
  }
}

object RDFResource {
  def apply(
      uri: String
  ): RDFResource = new RDFResource(IDGenerator.uri(uri))

  def apply(determinitisticBNodeId: Boolean): RDFResource = new RDFResource(
    if (determinitisticBNodeId) IDGenerator.deterministicBNodeId
    else IDGenerator.randomBNodeId
  )
}

object IDGenerator {
  var counter = -1
  def uri(v: String) = new URI(v)
  def deterministicBNodeId: BNodeID = {
    counter += 1
    new BNodeID(UUID.nameUUIDFromBytes(counter.toLong.toByteArray))
  }
  def randomBNodeId: BNodeID = new BNodeID(randomUUID)
}
