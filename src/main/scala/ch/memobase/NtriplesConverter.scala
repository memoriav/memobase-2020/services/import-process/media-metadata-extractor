/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import cats.implicits.*
import ch.memobase.models.*
import org.apache.logging.log4j.scala.Logging
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable
import scala.util.{Failure, Success, Try}
import java.util.TimeZone
import java.time.ZoneOffset
import org.joda.time.tz.FixedDateTimeZone
import org.joda.time.DateTimeZone

//noinspection HttpUrlsUsage
object NtriplesConverter extends Logging {

  /** Converts and filters a JSON object to a Ntriples representation
    *
    * @param jsonObj
    *   the JSON object received from the Indexer
    * @return
    *   relevant Ntriples to send downstream
    */
  // scalastyle:off
  def convertToNtriples(
      jsonObj: ujson.Value,
      ricoRecordId: String,
      ricoInstantiationId: String,
      serviceId: String,
      deterministicValues: Boolean = false
  ): (String, List[Throwable]) =
    jsonObj match {
      case o: ujson.Obj =>
        val (successes, failures) = o.value.iterator
          .collect {
            case ("errors", v: ujson.Obj) =>
              handleErrors(v).recoverWith { case e: Throwable =>
                logger.warn(
                  s"There was an error analysing the media file: ${e.getMessage}"
                )
                Failure(e)
              }
            case ("warnings", v: ujson.Obj) =>
              handleWarnings(v)
            case ("exif", v: ujson.Obj) =>
              handleExifData(
                v.value,
                ricoRecordId,
                ricoInstantiationId,
                serviceId,
                deterministicValues
              ).recoverWith { case e: Throwable =>
                logger.warn(
                  s"There was a problem when parsing the exif data: ${e.getMessage}"
                )
                Failure(e)
              }
            case ("ffprobe", v: ujson.Obj) =>
              handleFFprobeData(v.value, ricoRecordId, ricoInstantiationId)
                .recoverWith { case e: Throwable =>
                  logger.warn(
                    s"There was a problem when parsing the data from ffprobe: ${e.getMessage}"
                  )
                  Failure(e)
                }
            case ("height", v: ujson.Num) =>
              handleHeightData(v.value, ricoInstantiationId).recoverWith {
                case e: Throwable =>
                  logger.warn(
                    s"There was a problem when parsing the height field: ${e.getMessage}"
                  )
                  Failure(e)
              }
            case ("histogram", v: ujson.Arr) =>
              handleHistogramData(v.value, ricoRecordId, ricoInstantiationId)
                .recoverWith { case e: Throwable =>
                  logger.warn(
                    s"There was a problem when parsing the histogram data: ${e.getMessage}"
                  )
                  Failure(e)
                }
            case ("identify", v: ujson.Obj) =>
              handleIdentifyData(v.value, ricoRecordId, ricoInstantiationId)
                .recoverWith { case e: Throwable =>
                  logger.warn(
                    s"There was a problem when parsing the data from identify: ${e.getMessage}"
                  )
                  Failure(e)
                }
            case ("mimetype", v: ujson.Str) =>
              handleMimetypeData(v.value, ricoInstantiationId).recoverWith {
                case e: Throwable =>
                  logger.warn(
                    s"There was a problem when parsing the mimetype data: ${e.getMessage}"
                  )
                  Failure(e)
              }
            case ("siegfried", v: ujson.Arr) =>
              handleSiegfriedData(v.value, ricoRecordId, ricoInstantiationId)
                .recoverWith { case e: Throwable =>
                  logger.warn(
                    s"There was a problem when parsing the data from siegfried: ${e.getMessage}"
                  )
                  Failure(e)
                }
            case ("validateav", v: ujson.Obj) =>
              handleValidation(v.value).recoverWith { case e: Throwable =>
                logger.warn(
                  s"There was a problem when parsing the av validation data: ${e.getMessage}"
                )
                Failure(e)
              }
            case ("validateimage", v: ujson.Obj) =>
              handleValidation(v.value).recoverWith { case e: Throwable =>
                logger.warn(
                  s"There was a problem when parsing the image validation data: ${e.getMessage}"
                )
                Failure(e)
              }
            case ("width", v: ujson.Num) =>
              handleWidthData(v.value, ricoInstantiationId).recoverWith {
                case e: Throwable =>
                  logger.warn(
                    s"There was a problem when parsing the width field: ${e.getMessage}"
                  )
                  Failure(e)
              }
            case (p, v) if relevantProperties contains p =>
              logger.warn(
                s"JSON type ${v.toString} not expected for property `$p` in $ricoRecordId"
              )
              Failure(
                EnrichedMetadataException(
                  s"JSON type ${v.toString} not expected for property `$p` in $ricoRecordId"
                )
              )
          }
          .toList
          .partition(_.isSuccess)
        val ntripleStatements = successes.map(s => s.get).mkString.trim
        val errors = failures.map(f => f.failed.get)
        (ntripleStatements, errors)
      case _ => ("", List(EnrichedMetadataException("No valid JSON object")))
    }
  // scalastyle:on

  private def handleErrors(v: ujson.Obj): Try[String] = {
    if (v.value.nonEmpty) {
      Failure(ExtractionException(ujson.write(v)))
    } else {
      Success("")
    }
  }

  private def handleWarnings(v: ujson.Obj): Try[String] = {
    if (v.value.nonEmpty) {
      v.value.toList
        .map(w =>
          w._2 match {
            case s: ujson.Str => (w._1, s.value)
            case _            => (w._1, "<obj>")
          }
        )
        .foreach(w => logger.info(s"Weak indexer warning: ${w._1}: ${w._2}"))
    }
    Success("")
  }

  private def handleExifData(
      v: upickle.core.LinkedHashMap[String, ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String,
      serviceId: String,
      deterministicBNodeIds: Boolean
  ): Try[String] = {
    val affectedByActivity = createAffectedByRelation(ricoInstantiation)
    v.iterator
      .filter(t => exifProperties contains t._1)
      .map {
        case ("Artist", v: ujson.Str) =>
          createExifArtistNtriples(
            ricoRecord,
            v.value,
            serviceId,
            deterministicBNodeIds,
            affectedByActivity
          )
        case ("Copyright", v: ujson.Str) =>
          createExifCopyrightNtriples(
            ricoInstantiation,
            v.value,
            serviceId,
            deterministicBNodeIds,
            affectedByActivity
          )
        case ("CreatorCity", v: ujson.Str) =>
          createXmpCreatorPlaceNtriples(
            ricoRecord,
            v.value,
            serviceId,
            deterministicBNodeIds,
            affectedByActivity
          )
        case ("CreatorCountry", v: ujson.Str) =>
          createXmpCreatorPlaceNtriples(
            ricoRecord,
            v.value,
            serviceId,
            deterministicBNodeIds,
            affectedByActivity
          )
        case ("DateTimeOriginal", v: ujson.Str) =>
          createExifDateTimeOriginalNtriples(
            ricoInstantiation,
            v.value,
            serviceId,
            deterministicBNodeIds,
            affectedByActivity
          )
        case (k, v) =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${v.toString} not expected for property `exif.$k` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap(l => Success(l.mkString))
  }

  private def handleFFprobeData(
      v: upickle.core.LinkedHashMap[String, ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.iterator
      .filter(_._1 == "format")
      .map {
        case (_, v: ujson.Obj) =>
          Success(
            v.value.iterator
              .collect { case ("duration", v: ujson.Str) =>
                RDFResource(ricoInstantiation)
                  .addLiteral(ebucore("duration"), v.value)
                  .flush
              }
              .take(1)
              .mkString
          )
        case (_, v) =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${v.toString} not expected for property `ffprobe.streams` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap(l => Success(l.head))
  }

  private def handleHeightData(
      v: Double,
      ricoInstantiation: String
  ): Try[String] =
    Success(
      RDFResource(ricoInstantiation)
        .addLiteral(ebucore("height"), v.toString)
        .flush
    )

  private def handleHistogramData(
      v: mutable.ArrayBuffer[ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.iterator
      .map {
        case c: ujson.Obj if c.value.contains("code") =>
          Success(c.value("code"))
        case _: ujson.Obj =>
          Failure(
            EnrichedMetadataException(
              s"Property `histogram.<ind>.code` missing in $ricoRecord"
            )
          )
        case c =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${c.toString} not expected for property `histogram.<ind>` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap { l =>
        Success(
          l.map(c =>
            RDFResource(ricoInstantiation)
              .addLiteral(
                edm("componentColor"),
                c.value.toString.substring(1),
                "http://www.w3.org/2001/XMLSchema#hexBinary",
                DatatypeIndicator
              )
              .flush
          ).mkString
        )
      }
  }

  private def handleIdentifyData(
      v: upickle.core.LinkedHashMap[String, ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.iterator
      .filter(_._1 == "image")
      .map {
        case (_, v: ujson.Obj) =>
          Success(v.value.iterator.collect {
            case ("orientation", v: ujson.Str) =>
              RDFResource(ricoInstantiation)
                .addLiteral(ebucore("orientation"), v.value)
                .flush
            case ("type", v: ujson.Str) =>
              RDFResource(ricoInstantiation)
                .addLiteral(rdau("P60558"), v.value)
                .flush
          }.mkString)
        case (_, v) =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${v.toString} not expected for property `identify.image` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap(l =>
        if (l.nonEmpty) {
          Success(l.head)
        } else {
          Failure(
            EnrichedMetadataException(
              s"Property `identify.image` contains no inner objects"
            )
          )
        }
      )
  }

  private def handleMimetypeData(
      v: String,
      ricoInstantiation: String
  ): Try[String] = {
    Success(
      RDFResource(ricoInstantiation).addLiteral(ebucore("hasMimeType"), v).flush
    )
  }

  // scalastyle:off
  private def handleSiegfriedData(
      v: mutable.ArrayBuffer[ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.map {
      case e: ujson.Obj =>
        if (e.value.contains("id") || e.value.contains("ID")) {
          // Siegfried returns either variant
          val idValue =
            if (e.value.contains("id")) e.value("id") else e.value("ID")
          idValue match {
            case id: ujson.Str => Success(id.value)
            case _ =>
              Failure(
                EnrichedMetadataException(
                  s"Value of property `siegfried.<ind>.id` is not of type String"
                )
              )
          }
        } else {
          Failure(
            EnrichedMetadataException(
              s"No key `id` found in property siegfried in $ricoRecord"
            )
          )
        }
      case e =>
        Failure(
          EnrichedMetadataException(
            s"JSON type ${e.toString} not expected in property siegfried in $ricoRecord"
          )
        )
    }.toList
      .sequence
      .flatMap(l =>
        if (l.nonEmpty) {
          val id = l.get(0).get
          Success(
            RDFResource(ricoInstantiation)
              .addLiteral(ebucore("hasFormat"), id)
              .flush
          )
        } else {
          Failure(
            EnrichedMetadataException(
              s"Property `siegfried` contains no inner objects"
            )
          )
        }
      )
  }

  private def handleValidation(
      v: upickle.core.LinkedHashMap[String, ujson.Value]
  ): Try[String] =
    v match {
      case o if o.contains("status") && o("status").value == "ok" => Success("")
      case o if o.contains("status") && o.contains("message") =>
        Failure(
          ValidationException(
            s"Validation exception: ${o("message").value.toString}"
          )
        )
      case o if o.contains("status") =>
        Failure(ValidationException("Validation exception with unknown cause"))
      case _ => Success("")
    }

  private def handleWidthData(
      v: Double,
      ricoInstantiation: String
  ): Try[String] =
    Success(
      RDFResource(ricoInstantiation)
        .addLiteral(ebucore("width"), v.toString)
        .flush
    )

  private val relevantProperties = List(
    "errors",
    "exif",
    "ffprobe",
    "height",
    "histogram",
    "identify",
    "mimetype",
    "siegfried",
    "validateav",
    "validateimage",
    "width"
  )

  private val exifProperties = List(
    "Artist",
    "Copyright",
    "CreatorCity",
    "CreatorCountry",
    "DateTimeOriginal"
  )

  private val dct: String => String = (p: String) =>
    s"http://purl.org/dc/terms/$p"
  private val ebucore: String => String = (p: String) =>
    s"http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#$p"
  private val edm: String => String = (p: String) =>
    s"http://www.europeana.eu/schemas/edm/$p"
  private val rdau: String => String = (p: String) =>
    s"http://rdaregistry.info/Elements/u/$p"
  private val rdf: String => String = (p: String) =>
    s"http://www.w3.org/1999/02/22-rdf-syntax-ns#$p"
  private val rico: String => String = (p: String) =>
    s"https://www.ica.org/standards/RiC/ontology#$p"

  private def createRicoActivity(
      appId: String,
      referencingNodeId: RdfID,
      affectedNodeId: RdfID,
      deterministicValues: Boolean
  ) = {
    val rdfActivity = RDFResource(deterministicValues)

    val rdfMechanism = RDFResource(deterministicValues)
      .addURI(rdf("type"), rico("Mechanism"))
      .addResource(rico("performsOrPerformed"), rdfActivity)

    val now =
      if (deterministicValues) "1970-01-01T00:00:00.000Z"
      else new DateTime().toDateTime(DateTimeZone.UTC).toString

    rdfActivity
      .addURI(rdf("type"), rico("Activity"))
      .addURI(rico("resultsOrResultedIn"), referencingNodeId)
      .addURI(rico("affectsOrAffected"), affectedNodeId)
      .addLiteral(rico("name"), appId)
      .addLiteral(rico("type"), "enrichment")
      .addResource(rico("isOrWasPerformedBy"), rdfMechanism)
      .addLiteral(rico("beginningDate"), now)
      .addLiteral(rico("endDate"), now)
  }

  private val createAffectedByRelation = (instantiationId: String) =>
    (
      activity: RDFResource
    ) =>
      RDFResource(instantiationId)
        .addResource(
          rico("isOrWasAffectedBy"),
          activity
        )
        .flush

  private def createExifArtistNtriples(
      recordId: String,
      name: String,
      appId: String,
      deterministicBNodeIds: Boolean,
      affectedByActivity: RDFResource => String
  ) = {
    val creationRelationTarget = RDFResource(deterministicBNodeIds)
      .addLiteral(rico("name"), name)
      .addURI(rdf("type"), rico("Agent"))
    val rdfCreationRelation = RDFResource(deterministicBNodeIds)
      .addURI(rdf("type"), rico("CreationRelation"))
      .addLiteral(rico("type"), "exifArtist")
      .addResource(rico("creationRelationHasTarget"), creationRelationTarget)
    val activity =
      createRicoActivity(
        appId,
        rdfCreationRelation.uri,
        URI(recordId),
        deterministicBNodeIds
      )
    rdfCreationRelation
      .addResource(
        rico("resultsOrResultedFrom"),
        activity
      )
    Success(
      RDFResource(recordId)
        .addResource(
          rico("recordResourceOrInstantationIsSourceOfCreationRelation"),
          rdfCreationRelation
        )
        .flush + affectedByActivity(activity)
    )
  }

  private def createExifCopyrightNtriples(
      instantiationId: String,
      name: String,
      appId: String,
      deterministicBNodeId: Boolean,
      affectedByActivity: RDFResource => String
  ) = {
    val ruleResource = RDFResource(deterministicBNodeId)
      .addURI(rdf("type"), rico("Rule"))
      .addLiteral(rico("type"), "exifCopyright")
      .addLiteral(rico("name"), name)
      .addURI(rico("regulatesOrRegulated"), instantiationId)
    val copyright = RDFResource(instantiationId)
      .addURI(rico("isOrWasRegulatedBy"), ruleResource.uri)
    val activity = createRicoActivity(
      appId,
      ruleResource.uri,
      URI(instantiationId),
      deterministicBNodeId
    )
    Success(
      copyright.flush +
        ruleResource
          .addResource(
            rico("resultsOrResultedFrom"),
            activity
          )
          .flush + affectedByActivity(activity)
    )
  }

  private def createExifDateTimeOriginalNtriples(
      recordUri: String,
      dateTime: String,
      appId: String,
      deterministicBNodeId: Boolean,
      affectedByActivity: RDFResource => String
  ): Try[String] = {
    Try { exifDateTimeParser.parseDateTime(dateTime) }
      .orElse(Try(exifDateTimeTZParser.parseDateTime(dateTime)))
      .orElse(Try(exifDateParser.parseDateTime(dateTime)))
      .orElse(Try(new DateTime(dateTime))) match {
      case Success(dt) =>
        val dateResource = RDFResource(deterministicBNodeId)
          .addURI(rdf("type"), rico("SingleDate"))
          .addLiteral(rico("normalizedDateValue"), dt.toString)
        val created =
          RDFResource(recordUri).addURI(dct("created"), dateResource.uri)
        val activity =
          createRicoActivity(
            appId,
            dateResource.uri,
            URI(recordUri),
            deterministicBNodeId
          )
        Success(
          created.flush +
            dateResource
              .addResource(
                rico("resultsOrResultedFrom"),
                activity
              )
              .flush + affectedByActivity(activity)
        )
      case Failure(ex) => Failure(ex)
    }
  }

  private def createXmpCreatorPlaceNtriples(
      recordUri: String,
      placeName: String,
      appId: String,
      deterministicBNodeId: Boolean,
      affectedByActivity: RDFResource => String
  ): Try[String] = {
    val placeResource = RDFResource(deterministicBNodeId)
      .addURI(rdf("type"), rico("Place"))
      .addLiteral(rico("name"), placeName)
    val hasPlaceOfCapture =
      RDFResource(recordUri).addURI(rdau("P60556"), placeResource.uri)
    val activity =
      createRicoActivity(
        appId,
        placeResource.uri,
        URI(recordUri),
        deterministicBNodeId
      )
    Success(
      hasPlaceOfCapture.flush +
        placeResource
          .addResource(
            rico("resultsOrResultedFrom"),
            activity
          )
          .flush + affectedByActivity(activity)
    )
  }

  private val exifDateTimeParser =
    DateTimeFormat.forPattern("yyyy:MM:dd HH:mm:ss")

  private val exifDateTimeTZParser =
    DateTimeFormat.forPattern("yyyy:MM:dd HH:mm:ssZ")

  private val exifDateParser =
    DateTimeFormat.forPattern("yyyy:MM:dd")

}
