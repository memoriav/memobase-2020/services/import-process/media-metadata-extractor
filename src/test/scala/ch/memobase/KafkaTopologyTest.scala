/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.util.Properties
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.kafka.streams.scala.ImplicitConversions.*
import org.apache.kafka.streams.scala.*
import org.apache.kafka.streams.{KeyValue, StreamsConfig, TopologyTestDriver}
import org.scalatest.funsuite.AnyFunSuite

class KafkaTopologyTest extends AnyFunSuite {

  import org.apache.kafka.streams.scala.serialization.Serdes.*

  private val props: Properties = {
    val p = new Properties()
    p.put(StreamsConfig.APPLICATION_ID_CONFIG, "test")
    p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234")
    p
  }


  test("enrich record with analyzable media object successfully") {

    val builder: StreamsBuilder = new StreamsBuilder
    builder
      .stream[String, String]("input")
      .filter((_, v) => v.nonEmpty)
      .to("output")

    val topologyTestDriver = new TopologyTestDriver(builder.build(), props)
    val testInputTopic = topologyTestDriver.createInputTopic("input", new StringSerializer, new StringSerializer)
    testInputTopic.pipeInput("key", "value")
    val testOutputTopic = topologyTestDriver.createOutputTopic("output", new StringDeserializer, new StringDeserializer)
    assert(testOutputTopic.readKeyValue == new KeyValue("key", "value"))
    topologyTestDriver.close()
  }

}
