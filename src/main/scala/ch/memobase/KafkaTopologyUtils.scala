/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.*
import org.apache.logging.log4j.scala.Logging

import scala.util.{Success, Try}

object KafkaTopologyUtils extends Logging {

  import NtriplesConverter.*

  def enrichWithMetadata(
      msgWithLocator: (String, List[ResourceWithLocator]),
      metadataFetcher: MetadataFetcher,
      serviceName: String,
      throttlingRateForCollections: Map[String, Int]
  ): (String, List[Try[(String, List[Throwable])]]) = {
    val res = (
      msgWithLocator._1,
      LocatorExtraction
        .filterEnrichableResources(msgWithLocator._2)
        .map(rwl =>
          metadataFetcher
            .fetch(rwl.locator)
            .map(metadata =>
              convertToNtriples(
                metadata,
                rwl.recordId,
                rwl.resource,
                serviceName
              )
            )
        )
    )
    throttle(
      msgWithLocator._2.head.recordId,
      throttlingRateForCollections
    )
    res
  }

  def throttle(
      id: String,
      throttledCollection: Map[String, Int],
      simulated: Boolean = false
  ): Option[Int] = {
    val prefixLength = "https://memobase.ch/record/".length
    logger.debug(s"id: $id")
    logger.debug(s"throttled collections: ${throttledCollection.toList
      .map(x => s"${x._1} (${x._2} per minute)")
      .mkString(", ")}")
    Try(id.substring(prefixLength).split('-').take(2).mkString("-")) match {
      case Success(collectionId)
          if throttledCollection.contains(collectionId) =>
        val delay = 60000 / throttledCollection(collectionId)
        logger.debug(
          s"Throttled collection: Wait for $delay milliseconds before continue"
        )
        if (!simulated) {
          Thread.sleep(delay)
        }
        Some(delay)
      case _ =>
        logger.debug(s"Do not apply throttling")
        None
    }
  }
}
