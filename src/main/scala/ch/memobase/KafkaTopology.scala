/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.*
import ch.memobase.LocatorExtraction.*
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions.*
import org.apache.kafka.streams.scala.kstream.{Branched, KStream}
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.logging.log4j.scala.Logging
import org.apache.kafka.streams.kstream.Named

class KafkaTopology extends Logging with AppSettings {

  import KafkaTopologyUtils.*
  import Serdes.*
  logger.info(s"Current Step: $reportingStepName.")

  def build(metadataFetcher: MetadataFetcher): Topology = {
    val builder = new StreamsBuilder

    val source =
      builder.stream[String, String](kafkaInputTopic)

    // val Array(enrichedRecord, partiallyEnrichedRecord, unenrichedRecord) =
    val metadataEnriched =
      tryEnrichWithMetadata(source, metadataFetcher)

    metadataEnriched
      .get("enrichment-full")
      .foreach(sendEnrichedRecordsDownstream)
    metadataEnriched
      .get("enrichment-partial")
      .foreach(sendEnrichedRecordsDownstream)
    metadataEnriched
      .get("enrichment-failure")
      .foreach(sendUnenrichedRecordsDownstream)

    metadataEnriched.get("enrichment-full").foreach(reportEnrichedRecords)
    metadataEnriched
      .get("enrichment-partial")
      .foreach(reportRecordWithFailures)
    metadataEnriched
      .get("enrichment-failure")
      .foreach(reportRecordWithFailures)

    builder.build()
  }

  private def tryEnrichWithMetadata(
      stream: KStream[String, String],
      metadataFetcher: MetadataFetcher
  ) =
    stream
      .mapValues(v => (v, System.currentTimeMillis()))
      .mapValues((k, v) => {
        val resourceWithLocators = getEbucoreLocators(k, v._1)
        val enrichedMetadata =
          enrichWithMetadata(
            resourceWithLocators,
            metadataFetcher,
            reportingStepName,
            throttlingRateForCollections
          )
        ((enrichedMetadata._1, ProcessingResult(enrichedMetadata._2)), v._2)
      })
      .mapValues(v => {
        v
      })
      .split(Named.as("enrichment-"))
      .branch(
        (_, v) => v._1._2.isInstanceOf[ProcessingSuccess],
        Branched.as("full")
      )
      .branch(
        (_, v) => v._1._2.isInstanceOf[ProcessingPartialSuccess],
        Branched.as("partial")
      )
      .defaultBranch(Branched.as("failure"))

  private def sendRecordsDownstream(
      records: KStream[String, ((String, ProcessingResult), Long)]
  )(mappingFun: ((String, ProcessingResult)) => String): Unit =
    records
      .mapValues(v => mappingFun(v._1._1, v._1._2))
      .to(kafkaOutputTopic)

  private def sendEnrichedRecordsDownstream(
      enrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    sendRecordsDownstream(enrichedRecords)(x =>
      s"${x._1}\n${x._2.getStatements}"
    )

  private def sendUnenrichedRecordsDownstream(
      unenrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    sendRecordsDownstream(unenrichedRecords)(_._1)

  private def reportEnrichedRecords(
      enrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    enrichedRecords
      .mapValues((k, v) => {
        logger.info(
          s"Finished processing report $k in ${System.currentTimeMillis() - v._2}ms: SUCCESS (successfully enriched with metadata)"
        )
      })
      .map((k, _) =>
        (
          k,
          ReportingObject(
            k,
            ReportSuccess,
            "Record successfully enriched with metadata from media data."
          ).toString
        )
      )
      .to(kafkaReportTopic)

  private def reportRecordWithFailures(
      unenrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    unenrichedRecords
      .mapValues((k, v) => {
        logger.info(s"Finished processing report $k in ${System
            .currentTimeMillis() - v._2}ms: FAILURE (${v._1._2.getErrors})")
        v._1
      })
      .map((k, v) => {
        val exMsg = v._2.getErrors
        logger.warn(exMsg)
        logger.debug(v._2.errors.map(_.getStackTrace.mkString("\n")))
        (k, ReportingObject(k, ReportWarning, exMsg).toString)
      })
      .to(kafkaReportTopic)
}
