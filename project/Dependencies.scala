/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import sbt.*

object Dependencies {
  val http4sV = "0.23.27"
  val kafkaV = "3.8.0"
  val log4jV = "2.23.1"
  val rdf4jV = "5.0.2"
  val scalatestV = "3.2.18"

  lazy val cats = "org.typelevel" %% "cats-core" % "2.12.0"
  lazy val http4sDsl = "org.http4s" %% "http4s-dsl" % http4sV
  lazy val http4sServer = "org.http4s" %% "http4s-ember-server" % http4sV
  lazy val jodaTime = "joda-time" % "joda-time" % "2.12.7"
  lazy val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % kafkaV
  lazy val kafkaStreamsTestUtils =
    "org.apache.kafka" % "kafka-streams-test-utils" % kafkaV
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV
  lazy val log4jScala =
    "org.apache.logging.log4j" %% "log4j-api-scala" % "13.1.0"
  lazy val log4jSlf4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4jV
  lazy val mediaMetadataUtils =
    "ch.memobase" %% "media-metadata-utils" % "0.4.1"
  lazy val rdfRioApi = "org.eclipse.rdf4j" % "rdf4j-rio-api" % rdf4jV
  lazy val rdfRioNtriples = "org.eclipse.rdf4j" % "rdf4j-rio-ntriples" % rdf4jV
  lazy val requests = "com.lihaoyi" %% "requests" % "0.9.0"
  lazy val scalatic = "org.scalactic" %% "scalactic" % scalatestV
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalatestV
  lazy val scalaUri = "io.lemonlabs" %% "scala-uri" % "4.0.3"
  lazy val uPickle = "com.lihaoyi" %% "upickle" % "4.0.0"
}
