/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.models

import scala.util.Try

trait ProcessingResult {
  def getStatements: String = statements.mkString
  def getErrors: String = errors.map(_.getMessage).mkString("; ")
  val statements: List[String]
  val errors: List[Throwable]
}

object ProcessingResult {
  def apply(res: List[Try[(String, List[Throwable])]]): ProcessingResult = {
    res match {
      case r if isWithoutFailure(r) =>
        ProcessingSuccess(r.map(_.get._1))
      case r if hasEnrichmentErrors(r) =>
        ProcessingPartialSuccess(r.map(_.get._1), r.flatMap(_.get._2))
      case r if hasFetchingErrors(r) =>
        ProcessingPartialSuccess(
          r.filter(_.isSuccess).map(_.get._1),
          r.filter(_.isSuccess).flatMap(_.get._2) ++ r
            .filter(_.isFailure)
            .map(_.failed.get)
        )
      case r =>
        ProcessingFailure(
          errors = r.map(_.failed.get)
        )
    }
  }
  private def isWithoutFailure(
      res: List[Try[(String, List[Throwable])]]
  ): Boolean =
    res.forall(mediaObj => mediaObj.isSuccess && mediaObj.get._2.isEmpty)

  private def hasEnrichmentErrors(
      res: List[Try[(String, List[Throwable])]]
  ): Boolean =
    res.forall(_.isSuccess) && res.exists(_.get._2.nonEmpty)

  private def hasFetchingErrors(
      res: List[Try[(String, List[Throwable])]]
  ): Boolean =
    res.exists(_.isFailure) && !res.forall(_.isFailure)
}

case class ProcessingSuccess(
    statements: List[String],
    errors: List[Throwable] = List()
) extends ProcessingResult

case class ProcessingPartialSuccess(
    statements: List[String],
    errors: List[Throwable]
) extends ProcessingResult

case class ProcessingFailure(
    statements: List[String] = List(),
    errors: List[Throwable]
) extends ProcessingResult
