/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.*
import org.scalatest.funsuite.AnyFunSuite
import scala.language.reflectiveCalls

class NtriplesObjectTest extends AnyFunSuite {

  private val subject = "http://www.w3.org/2001/sw/RDFCore/ntriples/"
  private val predicate = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
  private val objUriLiteral = "http://xmlns.com/foaf/0.1/Document"
  private val objLiteral = "astring"
  private val objLiteralWithLinebreak: String =
    """astring
      |anotherString""".stripMargin
  private val objLiteralNormalized = """astring\nanotherString"""
  private val indicator = "en-US"

  test(
    "A single N-Triples statement with URI as object should have valid syntax"
  ) {
    val st = RDFResource(subject).addURI(predicate, objUriLiteral).flush
    assert(
      st == s"<${subject}> <${predicate}> <${objUriLiteral}> .\n"
    )
  }

  test(
    "A single N-Triples statement with literal as object should have valid syntax"
  ) {
    val st = RDFResource(subject).addLiteral(predicate, objLiteral).flush
    assert(
      st == s"""<${subject}> <${predicate}> "${objLiteral}" .\n"""
    )
  }

  test(
    "A single N-Triples statement with literal containing a linebreak as object should be normalized"
  ) {
    val st =
      RDFResource(subject).addLiteral(predicate, objLiteralWithLinebreak).flush
    assert(
      st == s"""<${subject}> <${predicate}> "${objLiteralNormalized}" .\n"""
    )
  }

  test(
    "A single N-Triples statement with annotated literal as object should have valid syntax"
  ) {
    val obj = Literal(objLiteral, indicator)
    val st =
      RDFResource(subject).addLiteral(predicate, objLiteral, indicator).flush
    assert(
      st == s"""<${subject}> <${predicate}> "${objLiteral}"@${indicator} .\n"""
    )
  }

  test("N-Triples statements with a blank node should have valid syntax") {
    val blankNode = RDFResource(false)
      .addURI(predicate, objUriLiteral)
      .addLiteral(predicate, objLiteral, indicator)
    val result = RDFResource(subject).addResource(predicate, blankNode)
    val hashCode = blankNode.uri
    val expected =
      s"""<${subject}> <${predicate}> $hashCode .
         |$hashCode <${predicate}> <${objUriLiteral}> .
         |$hashCode <${predicate}> "${objLiteral}"@${indicator} .\n""".stripMargin
    assert(result.flush == expected)
  }
}
