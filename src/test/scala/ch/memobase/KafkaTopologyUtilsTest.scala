/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.scalatest.funsuite.AnyFunSuite
import KafkaTopologyUtils.throttle

class KafkaTopologyUtilsTest extends AnyFunSuite {

  test("test throttling") {
    val throttledCollections = Map("afz-001" -> 2, "baz-001" -> 5)
    val throttledDocument = "https://memobase.ch/record/afz-001-23232-12323"
    assert(throttle(throttledDocument, throttledCollections, simulated = true).isDefined)
    val unhinderedDocument = "https://memobase.ch/record/arz-001-23232-12323"
    assert(throttle(unhinderedDocument, throttledCollections, simulated = true).isEmpty)
  }

}
