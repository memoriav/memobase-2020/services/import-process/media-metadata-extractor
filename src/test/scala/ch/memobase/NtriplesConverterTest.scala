/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.io.ByteArrayInputStream

import org.eclipse.rdf4j.rio.{RDFFormat, Rio}
import org.scalatest.funsuite.AnyFunSuite

import scala.jdk.CollectionConverters.*
import scala.io.Source
import scala.util.Try
import scala.language.reflectiveCalls
import ch.memobase.models.IDGenerator.deterministicBNodeId

class NtriplesConverterTest extends AnyFunSuite {

  import NtriplesConverter.*

  private def parseFileAsJson(path: String): ujson.Value =
    ujson.read(Source.fromFile(path).bufferedReader().lines().reduce(_ + _).get)

  private def readAndSortNtriples(path: String): String =
    Source
      .fromFile(path)
      .getLines
      .toList
      .map(_.stripTrailing)
      .sorted
      .mkString("\n")

  test("Parsing a JSON response should give valid n-triples statements") {
    val jsonObj =
      parseFileAsJson("src/test/resources/input/image_ok_1.json")
    val conversionRes = convertToNtriples(
      jsonObj,
      "https://memobase.ch/MEI-wef32",
      "https://memobase.ch/MEI-3wfwfe/digitalObject",
      "media-metadata-extractor"
    )

    val ntriples = conversionRes._1
    val errors = conversionRes._2

    val is = new ByteArrayInputStream(ntriples.getBytes)
    val checkedNtriples = Try {
      Rio.parse(is, "", RDFFormat.NTRIPLES)
    }
    assert(checkedNtriples.isSuccess)
    assert(errors.isEmpty)
  }

  test(
    "Parsing a JSON response should give a full, valid n-triples statements"
  ) {
    val jsonObj =
      parseFileAsJson("src/test/resources/input/image_ok_2.json")
    val conversionRes = convertToNtriples(
      jsonObj,
      "https://memobase.ch/zbz-002-99117048558005508",
      "https://memobase.ch/zbz-002-99117048558005508/digitalObject",
      "media-metadata-extractor",
      deterministicValues = true
    )

    val res =
      conversionRes._1.split("\n").map(_.stripTrailing).sorted.mkString("\n")
    val expected =
      readAndSortNtriples("src/test/resources/expected/image_ok_2.nt")

    assert(res == expected)
  }

  test(
    "Parsing a video object metadata response should keep a duration, width and height property"
  ) {
    val jsonObj =
      parseFileAsJson("src/test/resources/input/video_ok.json")
    val conversionRes = convertToNtriples(
      jsonObj,
      "https://memobase.ch/MEI-wef32",
      "https://memobase.ch/MEI-3wfwfe/digitalObject",
      "media-metadata-extractor"
    )

    val ntriples = conversionRes._1
    val errors = conversionRes._2

    val is = new ByteArrayInputStream(ntriples.getBytes)
    val ntriplesObj = Rio.parse(is, "", RDFFormat.NTRIPLES)
    val shouldContain =
      Set("height", "width", "hasMimeType", "duration", "hasFormat")
    assert(
      ntriplesObj
        .predicates()
        .asScala
        .map(_.getLocalName)
        .toSet
        .intersect(shouldContain)
        == shouldContain
    )
    assert(errors.isEmpty)
  }

  test(
    "Parsing a video object metadata with errors response should result in a partial enrichment"
  ) {
    val jsonObj =
      parseFileAsJson("src/test/resources/input/video_err.json")
    val conversionRes = convertToNtriples(
      jsonObj,
      "https://memobase.ch/MEI-wef32",
      "https://memobase.ch/MEI-3wfwfe/digitalObject",
      "media-metadata-extractor"
    )

    val ntriples = conversionRes._1
    val errors = conversionRes._2

    val is = new ByteArrayInputStream(ntriples.getBytes)
    val ntriplesObj = Rio.parse(is, "", RDFFormat.NTRIPLES)
    val shouldContain =
      Set("height", "width", "hasMimeType", "duration", "hasFormat")
    assert(
      ntriplesObj
        .predicates()
        .asScala
        .map(_.getLocalName)
        .toSet
        .intersect(shouldContain)
        == shouldContain
    )
    assert(errors.nonEmpty)
  }

  test(
    "Parsing image object metadata with an invalid datetimeoriginal stamp should nevertheless result in a partial enrichment"
  ) {
    val jsonObj =
      parseFileAsJson("src/test/resources/input/image_err_1.json")
    val conversionRes = convertToNtriples(
      jsonObj,
      "https://memobase.ch/MEI-wef32",
      "https://memobase.ch/MEI-3wfwfe/digitalObject",
      "media-metadata-extractor"
    )

    val ntriples = conversionRes._1
    val errors = conversionRes._2

    val is = new ByteArrayInputStream(ntriples.getBytes)
    val ntriplesObj = Rio.parse(is, "", RDFFormat.NTRIPLES)
    val shouldContain =
      Set("height", "width", "componentColor", "orientation", "P60558", "hasMimeType", "hasFormat")
    assert(
      ntriplesObj
        .predicates()
        .asScala
        .map(_.getLocalName)
        .toSet
        .intersect(shouldContain)
        == shouldContain
    )
    assert(errors.nonEmpty)
  }
}
