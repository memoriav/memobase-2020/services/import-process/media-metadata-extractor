import Dependencies.*

ThisBuild / scalaVersion := "3.5.2"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Media Metadata Extractor",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case "log4j.properties"                           => MergeStrategy.first
      case "log4j2.xml"                                 => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    assembly / mainClass := Some("ch.memobase.App"),
    git.useGitDescribe := true,
    Test / fork := true,
    Test / envVars := Map(
      "KAFKA_BOOTSTRAP_SERVERS" -> "localhost:9090",
      "APPLICATION_ID" -> "media-linker",
      "INDEXER_HOST" -> "localhost:8080",
      "TOPIC_IN" -> "in",
      "TOPIC_OUT" -> "out",
      "TOPIC_PROCESS" -> "report",
      "REPORTING_STEP_NAME" -> "media-metadata-extractor"
    ),
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      cats,
      http4sDsl,
      http4sServer,
      jodaTime,
      kafkaStreams.cross(CrossVersion.for3Use2_13),
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      mediaMetadataUtils excludeAll ExclusionRule(organization = "org.typelevel"),
      requests,
      scalatic,
      scalaUri excludeAll ExclusionRule(organization = "org.typelevel"),
      uPickle,
      kafkaStreamsTestUtils % Test,
      rdfRioApi % Test,
      rdfRioNtriples % Test,
      scalaTest % Test
    )
  )
